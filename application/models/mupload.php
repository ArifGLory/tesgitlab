<?php

class Mupload extends CI_Model
{
	var $gallerypath;
	var $gallery_path_url;
	
	function __construct()
	{
		$this->gallerypath = realpath(APPPATH .'../gambar');
		$this->gallery_path_url = base_url().'gambar/';
	}


function simpan_gambar(){
	$konfigurasi = array('alllowed_types'=>'jpg|jpeg|gif|png|bmp',
		'upload_path'=>$this->gallerypath);
	$this->load->library('upload',$konfigurasi);
	$this->upload->do_upload();
	$datafile = $this->upload->data();

	$konfigurasi =  array('source_image' => $datafile['full_path'] ,'new_image'=>$this->gallerypath .'/thumbnails',
					'maintain_ration'=>true,
					'width'=>200,
					'height'=>400);
	$this->load->library('image_lib',$konfigurasi);
	$this->image_lib->resize();

	$gambar = $_FILES['userfile']['name'];

	$data = array('nama_gambar' =>$gambar);
	$this->db->insert('tbl_gambar',$data);

}

	function tampil_gambar(){
		$query = $this->db->get('tbl_gambar');
		return $query;
	}
}
?>