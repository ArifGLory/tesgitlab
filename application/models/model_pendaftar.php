<?php
class model_pendaftar extends CI_Model{

    function list_pendaftar(){
        $data = $this->db->get('tbl_user');
        return $data;
    }
    function mahasiswa_daftar($npm){
        return $this->db->get_where('tbl_pendaftaran',array('npm'=>$npm));
    } 
   function list_surat(){
        $data = $this->db->get('tbl_surat');
        return $data;
    } 

    function filter_surat($bagian){
        
       // $this->db->like('bagian',$bagian);
        //$this->db->query("SELECT * from tbl_surat where tujuan = '$bagian' ");
       // $data = $this->db->get('tbl_surat');
     //   return $data;
        $data = $this->db->get_where('tbl_surat',array('tujuan'=>$bagian));
         return $data;

    
    }

    function mahasiswa_anggota($npm){
        return $this->db->get_where('tbl_anggota',array('npm'=>$npm));
    } 
}