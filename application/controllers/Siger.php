<?php
class Siger extends CI_Controller {

    var $gallery_path;
    var $gallery_path_url;

   public function __construct(){
        parent::__construct();
          $this->gallery_path = realpath(APPPATH . '../images');
          $this->gallery_path_url = base_url() . 'images/';
    }

	public function cari(){
        $this->load->model('model_cari');
        $keyword = $this->input->post('keyword');
        $data['daftar']    =   $this->model_cari->search($keyword);
        $this->load->view('main/cari_daftar',$data);
    }

     public function signin(){
        $id = $this->input->post('txt_id');
        $pass = $this->input->post('txt_password');
        $result = $this->db->select('*')->from('tbl_user')->where('id',$id)->limit(1)->get()->row();
        //$bagian = $this->db->select('bagian')->from('tbl_user')->where('id',$id)->limit(1)->get()->row();
        
        if($id == $result->id && $pass == $result->password){
        $this -> load -> model('model_pendaftar');
       // $data['data_surat'] = $this->model_pendaftar->list_surat()->result();
        $data['data_surat'] = $this->model_pendaftar->filter_surat($result->bagian)->result();
		$this-> load ->view('main/homeSiap',$data);
        }
        else{
           redirect('siger/gagal'); 
        }
    }

    public function beranda(){
    	 $this -> load -> model('model_pendaftar');
       // $data['data_surat'] = $this->model_pendaftar->list_surat()->result();
        $data['data_surat'] = $this->model_pendaftar->filter_surat($result->bagian)->result();
		$this-> load ->view('main/homeSiap',$data);
    }

    public function gagal(){
        $this->load->view('main/gagal_login');
    }

    public function tambahSurat(){

        $this-> load ->view('main/tambah_surat');
    }

    public function detilSurat(){
        $this-> load ->view('main/detil_surat');
    }

    public function hapus(){
        $id = $this->uri->segment(3);
       // $result = $this->db->select('foto')->from('tbl_pendaftaran')->where('npm',$npm)->limit(1)->get()->row();
        //unlink("./images/$result->foto");
        $this->db->where('id',$id);
        $this->db->delete('tbl_user');
        redirect('belajar/dataUser');
    }



     public function simpanUser(){
        
        if($this->input->post('daftar')){
            
            $config = array(
                'allowed_types' => 'jpg|jpeg|gif|png',
                'upload_path' => $this -> gallery_path,
                'max_size' => 2000
                );
                $this->load->library('upload',$config);
                $this->upload->do_upload('userfile');   
                $datapendaftar = array(
                        'id'=>$this->input->post('txt_id'),
                        'password'=>$this->input->post('txt_password'),
                        'nama'=>$this->input->post('txt_nama'),
                        'bagian'=>$this->input->post('txt_bagian'),
                        'status'=>$this->input->post('txt_status'),
                        'foto'=>$this->upload->file_name,
                        'tanggal'=>date('Y-m-d H:i:s')
                );
            }
             $this->db->insert('tbl_user',$datapendaftar);
       // $this->db->insert('tbl_user',$datapendaftar);
        redirect('Belajar/tambahUser');      
}

        public function simpanSurat(){
        
        if($this->input->post('tambah_surat')){
            
            $config = array(
                'allowed_types' => 'jpg|jpeg|gif|png',
                'upload_path' => $this -> gallery_path,
                'max_size' => 2000
                );
           // $this->load->library('upload',$config);
            //$this->upload->do_upload();
            //$datafile = $this->uplaod->data();
            $coba = $this->upload0>$file_name;
            echo "string $coba";
            
            $data_inputsurat = array(

                        'nosurat'=>$this->input->post('txt_nosurat'),
                        'perihal'=>$this->input->post('txt_perihal'),
                        'tanggal'=>$this->input->post('tanggal'),
                        'tujuan'=>$this->input->post('txt_tujuan'),
                        'jenis'=>$this->input->post('txt_jenis'),
              //          'gambar'=>$this->upload->$file_name,
                        //'minat'=>$this->input->post('txt_minat'),
                );
            }
        $this->db->insert('tbl_surat',$data_inputsurat);
        redirect('belajar/inputSurat');
        }


    public function inputSurat(){
        //echo "ini method coba pada controller belajar";
        $this -> load -> model('model_surat');
        $data['msurat'] = $this->model_surat->list_surat()->result();
        $this->load->view('main/tambah_surat',$data);  
        //$this->load->view('pendaftar');
    }

    public function load_kode(){
        $kode = $this->model_surat->get_kode_by_nama($this->response->post("nama"))->result();
  echo"<p><select name='kota' id='kota'><option value='' disabled selected>Pilih Kota</option>";
  foreach($kode as $kode_surat){
   echo"<option value='".$kode_surat->kode_surat."'>".$kode_surat->kode_surat."</option>";
  }     echo"</select></p><script>initComboBox();</script>";
      
    }

}
