<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Belajar extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
	}
 
	public function index(){
		
	}
 
	public function dataUser(){
		//echo "ini method coba pada controller belajar";
		$this -> load -> model('model_pendaftar');
        $data['daftar'] = $this->model_pendaftar->list_pendaftar()->result();
		$this->load->view('main/pendaftar',$data);  
		//$this->load->view('pendaftar');
	}

	public function tambahUser(){

		$this-> load ->view('main/daftar');
	}

	public function detailSurat(){
        $this-> load ->view('main/detil_surat');
    }

	public function home(){
		$this -> load -> model('model_pendaftar');
        $data['data_surat'] = $this->model_pendaftar->list_surat()->result();
		$this-> load ->view('main/homeSiap',$data);
	}

	 public function inputSurat(){

        $this-> load ->view('main/tambah_surat');
    }

 
}