<!doctype html>
<title>Error</title>
<style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 650px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>

<article>
    <h1>Telah terjadi kesalahan</h1>
    <div>
        <p>Anda gagal login, silahkan periksa User dan password anda !</p>
        <p>&mdash; S.I.A.P</p>
    </div>
</article>