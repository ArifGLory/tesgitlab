<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>
    <link rel="stylesheet" href="<?php echo base_url();?>/assets1/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets1/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets1/css/Google-Style-Login.css">
</head>

<body>
    <div id="layer1">
        <div class="login-card"><img src="<?php echo base_url();?>/assets1/img/logo_polda.png" class="profile-img-card">
            <p class="profile-name-card" style="color: #FFD700;"><strong>S.I.A.P</strong></p>
            
            <p class="profile-name-card" style="color:white;"><strong>Gagal Login, Periksa kembali User ID atau Password anda </strong></p>
           
             <p align="center" style="color:white; background-color:yellow"><a href="<?php echo base_url();?>index.php/login">Kembali ke halaman Login</a></p>
            <p align="center" style="color:white;"></p>
      
            <p align="center" style="color:white;">Copyright TapisDev</p>
        </div>
    </div>
    <script src="<?php echo base_url();?>/assets1/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>/assets1/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>