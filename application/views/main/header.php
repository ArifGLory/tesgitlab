<!DOCTYPE html>
<!--
	Transit by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Produk</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/skel.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/skel-layers.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="<?php echo base_url();?>assets/css/skel.css">
			<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
			<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style-xlarge.css">
		</noscript>
	</head>
	<body>

		<!-- Header -->
			<header id="header">
				<h1><a href="#">Indihome Lampung</a></h1>
				<nav id="nav">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Tarif dan harga</a></li>
						<li><a href="#">Produk</a></li>
						<li><a href="#" class="button special">Sign Up</a></li>
					</ul>
				</nav>
			</header>
