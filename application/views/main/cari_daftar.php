
<!DOCTYPE HTML>
<html>
<head>
<title>Admin Siger</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="SDM Polda Lampung" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>/assets2/css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>/assets2/css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="<?php echo base_url();?>/assets2/css/morris.css" type="text/css"/>
<!-- Graph CSS -->
<link href="<?php echo base_url();?>/assets2/css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<script src="<?php echo base_url();?>/assets2/js/jquery-2.1.4.min.js"></script>
<!-- //jQuery -->
<!-- tables -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets2/css/table-style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets2/css/basictable.css" />
<script type="text/javascript" src="<?php echo base_url();?>/assets2/js/jquery.basictable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
<!-- //tables -->
<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'/>
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!-- lined-icons -->
<link rel="stylesheet" href="<?php echo base_url();?>/assets2/css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
</head> 
<body>
   <div class="page-container">
   <!--/content-inner-->
<div class="left-content">
	   <div class="mother-grid-inner">
            <!--header start here-->
				<div class="header-main">
					
					<div class="w3layouts-left">
							
							<!--search-box-->
								<div class="w3-search-box">
									<?php echo form_open_multipart('siger/cari'); ?>   
										<input type="text" placeholder="Search..." required="" name="keyword" id="keyword">	
										<input type="submit" value="">					
									<?php echo form_close(); ?>
								</div><!--//end-search-box-->
							<div class="clearfix"> </div>
						 </div>
						 
						
				     <div class="clearfix"> </div>	
				</div>
<!--heder end here-->
	
				<!-- tables -->
				
				<div class="agile-tables">
					<h3>Data User</h3>
				  <table id="table-two-axis" class="two-axis">
					<thead>
					  <tr>
						<th>ID</th>
						<th>Password</th>
						<th>Nama</th>
						<th>Bagian</th>
						<th>Status</th>
						
					  </tr>
					</thead>
					<tbody>
                         <?php foreach ($daftar as $b){ ?>
                       <tr>
						<td><?php echo $b->id; ?></td>
						<td><?php echo $b->password; ?></td>
						<td><?php echo $b->nama; ?></td>
                        <td><?php echo $b->bagian; ?></td>
						<td><?php echo $b->status; ?></td>
						<!--<td><?php //echo '<img src="'.base_url().'images/'.$b->foto.'" width="100">'; ?></td>-->
                        <td><div><button  onclick="location.href= '<?php echo base_url(); ?>/index.php/siger/hapus/<?php echo $b->id; ?>'"><i class="ace-icon fa fa-trash-o bigger-120"></i></button> </div> <div><button  onclick="location.href = 'http://localhost/belajarCI/index.php/be/verifikasi/<?php echo $b->id; ?>'"><i class="ace-icon fa fa-check bigger-120"></i></button><?php } ?></div></td>
                          
					  </tr>
					  
					</tbody>
				  </table>

				</div>
				<!-- //tables -->
			</div>
<!-- script-for sticky-nav -->
              <script>
                function deletechecked(id)
                {
                    var answer = confirm("Akah anda ingin menghapus data dengan ID : "+id+" ?");
                    if (answer){
                    location.href = "http://localhost/belajarCI/index.php/siger/hapus/"+id;
                    }
                    return false;  
                    }

               </script>

		<script>
		$(document).ready(function() {
			 var navoffeset=$(".header-main").offset().top;
			 $(window).scroll(function(){
				var scrollpos=$(window).scrollTop(); 
				if(scrollpos >=navoffeset){
					$(".header-main").addClass("fixed");
				}else{
					$(".header-main").removeClass("fixed");
				}
			 });
			 
		});
		</script>
		<!-- /script-for sticky-nav -->
<!--inner block start here-->
<div class="inner-block">

</div>
<!--inner block end here-->
<!--copy rights start here-->
<div class="copyrights">
	 <p>© 2017 TapisDev | Supported by Glory </p>
</div>	
<!--COPY rights end here-->
</div>

  <!--//content-inner-->
		<!--/sidebar-menu-->
				<div class="sidebar-menu">
					<header class="logo1">
						<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> </a> 
					</header>
						<div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
                           <div class="menu">
									<ul id="menu" >
										<!--arif editing <li><a href="<?php echo base_url();?>index.php/masuk/admin"><i class="fa fa-file-text-o"></i> <span>Pendaftaran</span><div class="clearfix"></div></a></li>
                                        <li><a href="<?php echo base_url();?>index.php/masuk/anggota"><i class="fa fa-database"></i> <span>Data Anggota</span><div class="clearfix"></div></a></li>
								  -->
								  <li><a href="<?php echo base_url();?>index.php/belajar/dataUser"><i class="fa fa-file-text-o"></i> <span>Data User</span><div class="clearfix"></div></a></li>
								  <li><a href="<?php echo base_url();?>index.php/belajar/tambahUser"><i class="fa fa-file-text-o"></i> <span>Tambah User</span><div class="clearfix"></div></a></li>
								  <li><a href="<?php echo base_url();?>index.php/login"><i class="fa fa-file-text-o"></i> <span>Logout</span><div class="clearfix"></div></a></li>
								  </ul>
								</div>
							  </div>
							  <div class="clearfix"></div>		
							</div>
							<script>
							var toggle = true;
										
							$(".sidebar-icon").click(function() {                
							  if (toggle)
							  {
								$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
								$("#menu span").css({"position":"absolute"});
							  }
							  else
							  {
								$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
								setTimeout(function() {
								  $("#menu span").css({"position":"relative"});
								}, 400);
							  }
											
											toggle = !toggle;
										});
							</script>
<!--js -->
<script src="<?php echo base_url();?>/assets2/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url();?>/assets2/js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="<?php echo base_url();?>/assets2/js/bootstrap.min.js"></script>
   <!-- /Bootstrap Core JavaScript -->	   

</body>
</html>