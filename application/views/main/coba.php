<!DOCTYPE html>
<!--
	Transit by TEMPLATED
	templated.co @templatedco

	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Indihome Lampung</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
		<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/skel.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/skel-layers.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skel.css" />
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" />
			<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style-xlarge.css" />
		</noscript>
	</head>
	<body class="landing">

		<!-- Header -->
			<header id="header">
				<h1><a href="index.html">Indihome Lampung</a></h1>
				<nav id="nav">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Tarif dan harga</a></li>
						<li><a href="#">Produk</a></li>
						<li><a href="#" class="button special">Daftar</a></li>
					</ul>
				</nav>
			</header>

		<!-- Banner -->
			<section id="banner">
				<h2>Indihome Lampung</h2>
				<p>Jasa pemasangan Internet Indihome di Lampung</p>
				<ul class="actions">
					<li>
						<a href="#" class="button big">Daftar Pemasangan</a>
					</li>
				</ul>
			</section>

		<!-- One -->
			<section id="one" class="wrapper style1 special">
				<div class="container">
					<header class="major">
						<h2>Melayani Pemasangan Internet Wifi Kecepatan Tinggi</h2>
						<p>Saatnya Beralih Ke Teknologi Fiber Optic , Innternet Kecepatan tinggi </p>
					</header>
					<div class="row 150%">
						<div class="4u 12u$(medium)">
							<section class="box">
								<i class="icon big rounded color1 fa-home"></i>
								<h3>Internet Cepat</h3>
								<p>Layanan internet super cepat menggunakan fiber optik yang memiliki keunggulan cepat, stabil dan canggih.</p>
							</section>
						</div>
						<div class="4u 12u$(medium)">
							<section class="box">
								<i class="icon big rounded color9 fa-phone"></i>
								<h3>Telepon Rumah</h3>
								<p>Komunikasi telepon dengan keunggulan biaya nelpon lebih murah dan kualitas suara yang jernih.</p>
							</section>
						</div>
						<div class="4u$ 12u$(medium)">
							<section class="box">
								<i class="icon big rounded color6 fa-desktop"></i>
								<h3>IP TV ( Interaktif )</h3>
								<p>Pengalaman nonton lebih seru di UseeTV! Bisa playback hingga 7 hari dan bebas pilih channel TV favorit..</p>
							</section>
						</div>
					</div>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="wrapper style2 special">
				<div class="container">
					<header class="major">
						<h2>Jasa Pemasangan Internet Cepat</h2>
						<p>Kami dapat melayani anda untuk pemasangan Internet INDIHOME di Rumah/Kantor/Warnet ataupun Tempat usaha anda (Cafe, Showroom, Salon, Kos2an, Kontrakan, Dealer, Minimarket atau usaha lainnya) dengan Proses Cepat dan Mudah untuk area Bandar Lampung, Lampung dan sekitarnya</p>
					</header>

					<!--<section class="profiles">
						<div class="row">
							<section class="3u 6u(medium) 12u$(xsmall) profile">
								<img src="images/profile_placeholder.gif" alt="" />
								<h4>Lorem ipsum</h4>
								<p>Lorem ipsum dolor</p>
							</section>
							<section class="3u 6u$(medium) 12u$(xsmall) profile">
								<img src="images/profile_placeholder.gif" alt="" />
								<h4>Voluptatem dolores</h4>
								<p>Ullam nihil repudi</p>
							</section>
							<section class="3u 6u(medium) 12u$(xsmall) profile">
								<img src="images/profile_placeholder.gif" alt="" />
								<h4>Doloremque quo</h4>
								<p>Harum corrupti quia</p>
							</section>
							<section class="3u$ 6u$(medium) 12u$(xsmall) profile">
								<img src="images/profile_placeholder.gif" alt="" />
								<h4>Voluptatem dicta</h4>
								<p>Et natus sapiente</p>
							</section>
						</div>-->
					</section>
					<footer>
						<!--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam dolore illum, temporibus veritatis eligendi, aliquam, dolor enim itaque veniam aut eaque sequi qui quia vitae pariatur repudiandae ab dignissimos ex!</p>
						<ul class="actions">
							<li>
								<a href="#" class="button big">Lorem ipsum dolor sit</a>
							</li>
						</ul>-->
					</footer>
				</div>
			</section>

		<!-- Three --><!--
			<section id="three" class="wrapper style3 special">
				<div class="container">
					<header class="major">
						<h2>Consectetur adipisicing elit</h2>
						<p>Lorem ipsum dolor sit amet. Delectus consequatur, similique quia!</p>
					</header>
				</div>
				<div class="container 50%">
					<form action="#" method="post">
						<div class="row uniform">
							<div class="6u 12u$(small)">
								<input name="name" id="name" value="" placeholder="Name" type="text">
							</div>
							<div class="6u$ 12u$(small)">
								<input name="email" id="email" value="" placeholder="Email" type="email">
							</div>
							<div class="12u$">
								<textarea name="message" id="message" placeholder="Message" rows="6"></textarea>
							</div>
							<div class="12u$">
								<ul class="actions">
									<li><input value="Send Message" class="special big" type="submit"></li>
								</ul>
							</div>
						</div>
					</form>
				</div>
			</section>' -->

		<!-- Footer -->
			<footer id="footer">
				<div class="container">
					<section class="links">
						<div class="row">
							<section class="3u 6u(medium) 12u$(small)">
								<h3>Provinsi Lampung</h3>
								<ul class="unstyled">
									<li><a href="#">Telkom Indonesia</a></li>
									<li><img src="<?php echo base_url(); ?>assets/images/telkom.png" alt=""></img></li>
									
								</ul>
							</section>
							<!--<section class="3u 6u$(medium) 12u$(small)">
								<h3>Culpa quia, nesciunt</h3>
								<ul class="unstyled">
									<li><a href="#">Lorem ipsum dolor sit</a></li>
									<li><a href="#">Reiciendis dicta laboriosam enim</a></li>
									<li><a href="#">Corporis, non aut rerum</a></li>
									<li><a href="#">Laboriosam nulla voluptas, harum</a></li>
									<li><a href="#">Facere eligendi, inventore dolor</a></li>
								</ul>
							</section>-->
							<section class="3u 6u(medium) 12u$(small)">
								<h3>Indihome Lampung</h3>
								<ul class="unstyled">
									<li><i class="icon rounded color9 fa-phone">       WhatsApp</i></li>
									<li>+62822 8067 9630</li>
									
								</ul>
							</section>
							<!--<section class="3u$ 6u$(medium) 12u$(small)">
								<h3>Illum, tempori, saepe</h3>
								<ul class="unstyled">
									<li><a href="#">Lorem ipsum dolor sit</a></li>
									<li><a href="#">Recusandae, culpa necessita nam</a></li>
									<li><a href="#">Cupiditate, debitis adipisci blandi</a></li>
									<li><a href="#">Tempore nam, enim quia</a></li>
									<li><a href="#">Explicabo molestiae dolor labore</a></li>
								</ul>
							</section>-->
						</div>
					</section>
					<div class="row">
						<div class="8u 12u$(medium)">
							<ul class="copyright">
								<li>&copy; TapisDev. All rights reserved.</li>
								<li>Design: <a href="#">Arif Glory</a></li>
								<li>Images: <a href="#">Google</a></li>
							</ul>
						</div>
						<div class="4u$ 12u$(medium)">
							<ul class="icons">
								<li>
									<a class="icon rounded fa-facebook"><span class="label">Facebook</span></a>
								</li>
								<li>
									<a class="icon rounded fa-twitter"><span class="label">Twitter</span></a>
								</li>
								<li>
									<a class="icon rounded fa-google-plus"><span class="label">Google+</span></a>
								</li>
								<li>
									<a class="icon rounded fa-linkedin"><span class="label">LinkedIn</span></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</footer>

	</body>
</html>