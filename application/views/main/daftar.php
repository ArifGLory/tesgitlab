<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>SIAPP - SI Persuratan Polda</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets3/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets3/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets3/css/animate.css">
	<link href="<?php echo base_url();?>/assets3/css/animate.min.css" rel="stylesheet"> 
	<link href="<?php echo base_url();?>/assets3/css/style.css" rel="stylesheet" />	
      
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
    
  </head>
  <body>	
	<header id="header">
        <nav class="navbar navbar-default navbar-static-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                   <div class="navbar-brand">
						<a href="index.html"><h1>SIAPP</h1></a>
					</div>
                </div>				
                <div class="navbar-collapse collapse">							
					<div class="menu">
						<ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="<?php echo base_url();?>/index.php/belajar/dataUser" >Data User</a></li>
							<li role="presentation"><a href="<?php echo base_url();?>/index.php/belajar/tambahUser">Tambah User</a></li>   
                            <li role="presentation"><a href="<?php echo base_url();?>/index.php/login">Logout</a></li>
							
						</ul>
					</div>
				</div>		
            </div><!--/.container-->
        </nav><!--/nav-->		
    </header><!--/header-->	
	
      
	<section class="contact-page">
        <div class="container">
            <div class="text-center">        
                <h2 class="text-center" id="judul"><strong><span style="text-decoration: underline;">Form Penambahan User</span></strong></h2>
            </div> 
            <div id="back">
            <div id="formdiv">
            <?php echo form_open_multipart('Protek/simpan'); ?>                
                <div class="form-group">
                    <label class="control-label" for="txt_id">ID </label>
                    <input class="form-control" type="text" name="txt_id" placeholder="User ID" id="txt_id">
                </div>
                <div class="form-group">
                    <label class="control-label" for="txt_password">Password </label>
                    <input class="form-control" type="password" name="txt_password" placeholder="Password" id="txt_password">
                </div>
                <div class="form-group">
                    <label class="control-label" for="txt_nama">Nama Lengkap </label>
                    <input class="form-control" type="text" name="txt_nama" placeholder="Nama " id="txt_nama">
                </div>
                <div class="form-group">
                    <label class="control-label" for="txt_status">Level Status</label>
                    <select class="form-control" name="txt_status">
                        <option value="">Pilih Status</option>
                        <option value="admin">Admin</option>
                        <option value="user">User</option>
                    </select>
                </div>
               
                <div class="form-group">
                    <label class="control-label" for="txt_bagian">Bidang</label>
                    <select class="form-control" name="txt_bagian">
                        <option value="">Pilih Bidang</option>
                        <option value="Karo">Karo SDM</option>
                        <option value="Renmin">Renmin</option>
                        <option value="Dalpres">Dalpres</option>
                        <option value="Binkar">Binkar</option>
                        <option value="Watpers">Watpers</option>
                        <option value="Psikologi">Psikologi</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="txt_foto">Pas Foto *</label>
                   <input type="file" name="userfile" value="foto"  />
                </div>
                <!--<div class="form-group">
                    <label class="control-label" for="txt_foto">Pas Foto *</label>
                   <input type="file" name="userfile" value="foto"  />
                </div>-->
                <div class="form-group" id="fgbutton">
                    <button class="btn btn-primary btn-lg" type="submit" name="daftar" value="daftar" id="daftar">Tambah</button>
                </div>
                
            <?php echo form_close(); ?>
            </div>
            </div>
        </div><!--/.container-->
    </section><!--/#contact-page-->
	
	
	<footer>
		<div class="container">
			<div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
				<h4>Tentang</h4>
				<p>Sistem Informasi Administrasi Persuratan - SIAP</p>						
				<div class="contact-info">
					<ul>
						<li><i class="fa fa-home fa"></i>Jl.ZA Pagar Alam No.9 -11, Bandar Lampung ( Universitas Teknokrat Indonesia)</li>
						<li><i class="fa fa-phone fa"></i> 089662240052 (Arif Rahman Edison)</li>
						<li><i></i> www.ukmprotek.id</li>
					</ul>
				</div>
			</div>
			
			
			
        </div>
			
		</div>	
	</footer>
	
	<div class="sub-footer">
		<div class="container">
			<div class="social-icon">
				<div class="col-md-4">
					
				</div>
			</div>
			
			<div class="col-md-4 col-md-offset-4">
				<div class="copyright">
					&copy; PROTEK
                    <div class="credits">
                        <!-- 
                            All the links in the footer should remain intact. 
                            You can delete the links only if you purchased the pro version.
                            Licensing information: https://bootstrapmade.com/license/
                            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Day
                        -->
                        <a >Supported</a> by <a>TapisDev</a>
                       
                    </div>
				</div>
			</div>						
		</div>				
	</div>
	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-y7ZBc-OqiFqwbiGM8XcjXSeyiCMCJxM&callback=initMap"></script>	
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>/assets3/js/bootstrap.min.js"></script>	
    <script src="https://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo base_url();?>/assets3/js/wow.min.js"></script>
	<script>wow = new WOW({}).init();</script>	
	<!--<script>
        function ($) {
            //Google Map
            var get_latitude = $('#google-map').data('latitude');
            var get_longitude = $('#google-map').data('longitude');
            
            function initialize_google_map() {
                var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
                var mapOptions = {
                    zoom: 14,
                    scrollwheel: false,
                    center: myLatlng
                };
                var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });
            }
            google.maps.event.addDomListener(window, 'load', initialize_google_map);
        })(jQuery);
    </script>	-->
        
    <script src="contactform/contactform.js"></script>
    
</body>
</html>