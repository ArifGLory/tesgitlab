
		<!-- Main -->
			<section id="main" class="wrapper">
				<div class="container">

					<header class="major">
						<h2>Produk Kami  </h2>
						<p>Layanan yang anda bisa dapatkan dari Indihome</p>
					</header>

					<a href="#" class="image fit"><img src="<?php echo base_url(); ?>assets/images/back2.png" alt="" /></a>
					
					<div class="row 150%">
						<div class="4u 12u$(medium)">
							<section class="box">
								<i class="icon big rounded color1 fa-home"></i>
								<h3>Internet Cepat</h3>
								<p>Layanan internet super cepat menggunakan fiber optik yang memiliki keunggulan cepat, stabil dan canggih.</p>
							</section>
						</div>
						<div class="4u 12u$(medium)">
							<section class="box">
								<i class="icon big rounded color9 fa-phone"></i>
								<h3>Telepon Rumah</h3>
								<p>Komunikasi telepon dengan keunggulan biaya nelpon lebih murah dan kualitas suara yang jernih.</p>
							</section>
						</div>
						<div class="4u$ 12u$(medium)">
							<section class="box">
								<i class="icon big rounded color6 fa-desktop"></i>
								<h3>IP TV ( Interaktif )</h3>
								<p>Pengalaman nonton lebih seru di UseeTV! Bisa playback hingga 7 hari dan bebas pilih channel TV favorit..</p>
							</section>
						</div>
						<!-- line 2 -->
						<div class="4u 12u$(medium)">
							<section class="box">
								<i class="icon big rounded color1 fa-flash"></i>
								<h3>IFlix</h3>
								<p>Layanan internet super cepat menggunakan fiber optik yang memiliki keunggulan cepat, stabil dan canggih.</p>
							</section>
						</div>
						<div class="4u 12u$(medium)">
							<section class="box">
								<i class="icon big rounded color9 fa-file-movie-o"></i>
								<h3>CatchPlay</h3>
								<p>Komunikasi telepon dengan keunggulan biaya nelpon lebih murah dan kualitas suara yang jernih.</p>
							</section>
						</div>
						<div class="4u$ 12u$(medium)">
							<section class="box">
								<i class="icon big rounded color6 fa-mobile"></i>
								<h3>Movin</h3>
								<p>Pengalaman nonton lebih seru di UseeTV! Bisa playback hingga 7 hari dan bebas pilih channel TV favorit..</p>
							</section>
						</div>
					</div>

				</div>
			</section>
