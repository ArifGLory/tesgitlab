<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login S.I.A.P</title>
    <link rel="stylesheet" href="<?php echo base_url();?>/assets1/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets1/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url();?>/assets1/css/Google-Style-Login.css">
</head>

<body>
    <div id="layer1">
        <div class="login-card"><img src="<?php echo base_url();?>/assets1/img/logo_polda.png" class="profile-img-card">
            <p class="profile-name-card" style="color: #FFD700;"><strong>S.I.A.P</strong></p>
            <p class="profile-name-card" style="color:white;"><strong>Sign In</strong></p>
            <p align="center" style="color:white;">Sistem Informasi Administrasi Persuratan</p>
            <p align="center" style="color:white;">Biro SDM Polda Lampung</p>

            <form class="form-signin" action="<?php echo base_url();?>/index.php/siger/signin" enctype="multipart/form-data" method="post" accept-charset="utf-8"><span class="reauth-email"> </span>
                <input class="form-control" type="text" required="" placeholder="User ID" autofocus="" id="txt_id" name="txt_id">
                <input class="form-control" type="password" required="" placeholder="Password" id="txt_password" name="txt_password">
                <div class="checkbox"></div>
                <button class="btn btn-primary btn-block btn-lg btn-signin" type="submit">Sign in</button>
            </form>
            <p align="center" style="color:white;"></p>
            <p align="center" style="color:white;">Copyright TapisDev</p>
        </div>
    </div>
    <script src="<?php echo base_url();?>/assets1/js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>/assets1/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>