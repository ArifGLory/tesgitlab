<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>S.I.A.P - Input Surat</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>/assets3/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets3/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>/assets3/css/animate.css">
	<link href="<?php echo base_url();?>/assets3/css/animate.min.css" rel="stylesheet"> 
	<link href="<?php echo base_url();?>/assets3/css/style.css" rel="stylesheet" />	
    <link rel="stylesheet" href="<?php echo base_url();?>/assets1/css/Google-Style-Login.css">
      
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600' rel='stylesheet' type='text/css'>
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.1.1/css/font-awesome.css" rel="stylesheet">
    
  </head>
  <body>	
    <?php 
    $kode_surat = "";
     ?>
	<header id="header">
        <nav class="navbar navbar-default navbar-static-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                   <div class="navbar-brand">
						<a href="#"><h1>S.I.A.P</h1></a>
                        
                        <img src="<?php echo base_url();?>/assets1/img/logo_polri2.png" class="gambar-kecil" align="right">
					</div>
                </div>				
                <div class="navbar-collapse collapse">							
					<div class="menu">
						<ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="<?php echo base_url();?>/index.php/belajar/home" >Data Surat</a></li>
							<li role="presentation"><a href="<?php echo base_url();?>/index.php/siger/tambahSurat">Tambah Surat</a></li>   
                            <li role="presentation"><a href="<?php echo base_url();?>/index.php/login">Logout</a></li>
							
						</ul>
					</div>
				</div>		
            </div><!--/.container-->
        </nav><!--/nav-->		
    </header><!--/header-->	
	
      
	<section class="contact-page">
        <div class="container">
            <div class="text-center">        
                <h2 class="text-center" id="judul"><strong><span style="text-decoration: underline;">Form Input Surat</span></strong></h2>
            </div> 
            <div id="back">
            <div id="formdiv">
             <div class="form-group">
                    <?php $pilihan = 0; ?> 
                    <div class="form-group">
                    <form method="post" action="">
                    <label class="control-label" for="jenis">Jenis Surat</label>
                    <select class="form-control" name="jenis " id="jenis" onchange="myFunction">
                        <option value="" disabled>Pilih Jenis Surat</option>
                         <?php foreach ($msurat as $c){ ?>
                        <option> <?php echo $c->jenis_surat ?></option>
                        <?php } ?>
                        <!--
                       -->
                       <!-- <option value="BIASA">Biasa</option>
                        <option value="TELEGRAM">Telegram</option>
                        <option value="TELEGRAM RAHASIA">Telegram Rahasia</option>
                        <option value="NOTA DINAS">Nota Dinas</option>
                        <option value="RAHASIA">Rahasia</option>
                        <option value="SPRIN">Surat Perintah</option>
                    -->
                    </select>
                    </form>
                </div>
<script>
function myFunction() {
    var x = document.getElementById("jenis");
    var no_surat = document.getElementById("txt_nosurat");
    if (x.value = "RAHASIA") {
        x.value = x.value.toLowerCase();
    }
    //x.value = x.value.toUpperCase();
}
</script>
<script>
function setKode(){
         var kode= $('#jenis').val();
  $.ajax({
   type: 'POST',
   data: "nama="+nama,
   url: 'siger/load_kode/',
   success: function(result) {
    $('#no_surat').html(result);       }
  });
 }
</script>

            <?php echo form_open_multipart('siger/simpanSurat'); ?>                

                    <label class="control-label" for="lbl_no_surat" name="lbl_no_surat" id="lbl_no_surat">No. Surat</label>
                    <input class="form-control" type="text" name="txt_nosurat" placeholder="No. Surat" id="txt_nosurat" value="">
                    <div id="no_surat">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label" for="txt_perihal">Perihal</label>
                    <input class="form-control" type="text" name="txt_perihal" placeholder="Perihal" id="txt_perihal">
                </div>
                <div class="form-group">
                    <label class="control-label" for="txt_perihal">Tanggal</label>
                    <input class="form-control" type="text" name="txt_tanggal" placeholder="Tanggal" id="txt_tanggal" value="<?php print(date('m-d-Y')) ?>" disabled>
                </div>
                <div class="form-group">
                    <label class="control-label" for="txt_tujuan">Tujuan</label>
                    <select class="form-control" name="txt_tujuan">
                        <option value="">Pilih Tujuan</option>
                        <option value="Karo SDM">Karo SDM</option>
                        <option value="Dalpres">Dalpres</option>
                        <option value="Renmin">Renmin</option>
                        <option value="Binkar">Binkar</option>
                        <option value="Watpers">Watpers</option>
                        <option value="Psikologi">Psikologi</option>
                    </select>
                </div>
               
                <div class="form-group">
                    <label class="control-label" for="txt_scan">Scan Surat (File Gambar)</label>
                   <input type="file" name="userfile" value="gambar"  />
                </div>
                <div class="form-group" id="fgbutton">
                    <button class="btn btn-primary btn-lg" type="submit" name="tambah_surat" value="tambah_surat" id="tambah_surat">Tambah</button>
                </div>
                <!--<div>
                  <?php// print(date('Y-m-d H:i:s')) ?>      
                </div>  -->
                
            <?php echo form_close(); ?>
            </div>
            </div>
        </div><!--/.container-->
    </section><!--/#contact-page-->

	
	
	<footer>
        <div>
            <img src="<?php echo base_url();?>/assets1/img/logo_sdm.png" class="gambar-sedang" align="right">
        </div>
		<div class="container">
			<div class="col-md-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
				<h4>Tentang</h4>
				<p>Sistem Informasi Administrasi Persuratan - S.I.A.P</p>						
				<div class="contact-info">
					<ul>

						<li>Kepolisian Negara Republik Indonesia</li>
                        <li>Biro SDM Polda Lampung</li>
					</ul>
				</div>
			</div>
			
			
        </div>
			
		</div>	
	</footer>
	
	<div class="sub-footer">
		<div class="container">
			<div class="social-icon">
				<div class="col-md-4">
					
				</div>
			</div>
			
			<div class="col-md-4 col-md-offset-4">
				<div class="copyright">
					&copy; TapisDev
                    <div class="credits">
                        <!-- 
                            All the links in the footer should remain intact. 
                            You can delete the links only if you purchased the pro version.
                            Licensing information: https://bootstrapmade.com/license/
                            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Day
                        -->
                        <a >Supported</a> by <a>Arif Glory</a>
                        <a >+6289662240052 (WA) </a>
                       
                    </div>
				</div>
			</div>						
		</div>				
	</div>
	
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-y7ZBc-OqiFqwbiGM8XcjXSeyiCMCJxM&callback=initMap"></script>	
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>/assets3/js/bootstrap.min.js"></script>	
    <script src="https://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="<?php echo base_url();?>/assets3/js/wow.min.js"></script>
	<script>wow = new WOW({}).init();</script>	
	<!--<script>
        function ($) {
            //Google Map
            var get_latitude = $('#google-map').data('latitude');
            var get_longitude = $('#google-map').data('longitude');
            
            function initialize_google_map() {
                var myLatlng = new google.maps.LatLng(get_latitude, get_longitude);
                var mapOptions = {
                    zoom: 14,
                    scrollwheel: false,
                    center: myLatlng
                };
                var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });
            }
            google.maps.event.addDomListener(window, 'load', initialize_google_map);
        })(jQuery);
    </script>	-->
        
    <script src="contactform/contactform.js"></script>
    
</body>
</html>